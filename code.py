# SPDX-FileCopyrightText: 2018 Kattni Rembor for Adafruit Industries
#
# SPDX-License-Identifier: MIT
import stamp_round_carrier_board as board



"""CircuitPython Essentials NeoPixel example"""
import time
from rainbowio import colorwheel
import neopixel

print("Hello World")

REPEAT=5
DELAY_CHASE=0.05


pixel_pin = board.NEOPIXEL
num_pixels = 16

pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness=0.3, auto_write=False)

def turnoff():
    show_color((0,0,0), 0)

def color_chase(color, wait):
    for i in range(num_pixels):
        pixels[i] = color
        time.sleep(wait)
        pixels.show()
    time.sleep(0.5)

def show_color(color, wait):
    pixels.fill(color)
    pixels.show()
    time.sleep(wait)


def rainbow_cycle(wait):
    for j in range(255):
        for i in range(num_pixels):
            rc_index = (i * 256 // num_pixels) + j
            pixels[i] = colorwheel(rc_index & 255)
        pixels.show()
        time.sleep(wait)


RED = (255, 0, 0)
YELLOW = (255, 150, 0)
GREEN = (0, 255, 0)
CYAN = (0, 255, 255)
BLUE = (0, 0, 255)
PURPLE = (180, 0, 255)

for x in range(REPEAT):
    show_color(RED, 0.5)
    show_color(GREEN, 0.5)
    show_color(BLUE, 0.5)
    
    color_chase(RED, DELAY_CHASE)  # Increase the number to slow down the color chase
    color_chase(YELLOW, DELAY_CHASE)
    color_chase(GREEN, DELAY_CHASE)
    color_chase(CYAN, DELAY_CHASE)
    color_chase(BLUE, DELAY_CHASE)
    color_chase(PURPLE, DELAY_CHASE)

    rainbow_cycle(0)  # Increase the number to slow down the rainbow

turnoff()